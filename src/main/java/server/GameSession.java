package server;

import ais.SheepRandomBot;
import board.Board;
import chesspieces.Wolf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GameSession implements Runnable {

	private final Socket socket;
	private Board board;
	private final SheepRandomBot game;
	private PrintWriter out;
	private BufferedReader in;

	public GameSession(Socket socket) {
		this.socket = socket;
		board = new Board();
		game = new SheepRandomBot(board);
	}

	public void run() {
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			String command;
			update();
			while (!socket.isClosed() && (command = in.readLine()) != null) {
				String[] parsed = command.split(Command.SEPARATOR);
				System.out.println("From server:" + command);

				if (Command.SELECT_WOLF.name().equals(parsed[0])) {
					int wolf = Integer.parseInt(parsed[1]);
					submitPossibleWays(wolf);
				}

				if (Command.MOVE_WOLF.name().equals(parsed[0])) {
					int wolfIndex = Integer.parseInt(parsed[1]);
					int wolfC = Integer.parseInt(parsed[2]);

					Wolf wolf = board.getWolf()[wolfIndex];
					int wolfAlterPos = wolf.moveRight() == wolfC ? wolf.moveLeft() : wolf.moveRight();
					game.movementWolf(wolf.moveDown(), wolfC, wolfAlterPos, board, wolfIndex);
					update();
					if(game.isEnd(board)){
						int res = game.isWolfWon() ? 1 : 0;

						out.print(Command.END);
						out.print(Command.SEPARATOR);
						out.println(res);
						out.flush();

						socket.close();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			try {
				socket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}

	}

	private void submitPossibleWays(int wolfIndex) {
		out.print(Command.POSSIBLE_WOLF_MOVES.name());
		Wolf wolf = board.getWolf()[wolfIndex];
		out.print(Command.SEPARATOR);
		out.print(wolfIndex);
		if (game.moveIsPossible(wolf.moveDown(), wolf.moveLeft())) {
			out.print(Command.SEPARATOR);
			out.print(wolf.moveDown());
			out.print(Command.SEPARATOR);
			out.print(wolf.moveLeft());
		}
		if (game.moveIsPossible(wolf.moveDown(), wolf.moveRight())) {
			out.print(Command.SEPARATOR);
			out.print(wolf.moveDown());
			out.print(Command.SEPARATOR);
			out.print(wolf.moveRight());
		}
		out.println();
		out.flush();
	}

	private void update() {
		out.print(Command.UPDATE.name());
		out.print(Command.SEPARATOR);
		out.print(board.getSheep().getRowPosition());
		out.print(Command.SEPARATOR);
		out.print(board.getSheep().getColPosition());
		for (Wolf wolf : board.getWolf()) {
			out.print(Command.SEPARATOR);
			out.print(wolf.getRowPosition());
			out.print(Command.SEPARATOR);
			out.print(wolf.getColPosition());
		}
		out.println();
		out.flush();
	}

}