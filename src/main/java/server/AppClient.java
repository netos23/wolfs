package server;



import ais.ClientController;
import ais.SheepRandomBot;
import board.Board;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import menu.MenuPanel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class AppClient extends Application {

    public static void main(String[] args) throws IOException {
        launch(args);
    }



    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Board board = new Board();

        ClientController controller = new ClientController(board,"127.0.0.1",8080);
        MenuPanel menuPanel = new MenuPanel(root, controller, primaryStage);

//        Game game = new Game(board);
//        game.play(board);
//        game.moveSheep(board);

        /*SheepRandomBot gameBot = new SheepRandomBot(board);
        gameBot.moveSheep(board);
        gameBot.moveWolf(board);*/

//        Player p1 = new Player(board);


//        BotGame game2 = new BotGame(board, primaryStage);
//        game2.moveSheep(board, primaryStage);



        menuPanel.getExitButton().setOnAction(actionEvent -> {
            primaryStage.close();
        });
        primaryStage.setTitle("Wolf and Sheep");
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.show();
    }
}