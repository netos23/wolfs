package server;

public enum Command {
	GAME,
	SELECT_WOLF,
	POSSIBLE_WOLF_MOVES,
	MOVE_WOLF,
	UPDATE,
	END;

	public static final String SEPARATOR = ":";
}