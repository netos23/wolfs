package ais;

import board.Board;
import board.BoardSquare;
import chesspieces.Shape;
import chesspieces.Sheep;
import chesspieces.Wolf;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import server.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientController implements Runnable {
	private final Board board;

	private final String host;
	private final int port;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;

	public ClientController(Board board, String host, int port) {
		this.board = board;
		this.host = host;
		this.port = port;
	}

	public void start() throws IOException {
		socket = new Socket(host, port);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream());
		bindWolfs();
		String command;
		while (!socket.isClosed() && (command = in.readLine()) != null) {
			String[] parsed = command.split(Command.SEPARATOR);
			System.out.println("From server:" + command);

			Platform.runLater(() -> {
				if (Command.END.name().equals(parsed[0])) {
					Shape.MonsterType winner = Integer.parseInt(parsed[1]) == 0
							? Shape.MonsterType.SHEEP
							: Shape.MonsterType.WOLF;
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					showWinner(winner);
				}

				if (Command.POSSIBLE_WOLF_MOVES.name().equals(parsed[0])) {
					int wolf = Integer.parseInt(parsed[1]);
					int[] possibleMovesCoords = readPositions(parsed, 2);
					highlightPossibleMoves(wolf, possibleMovesCoords);
				}

				if (Command.UPDATE.name().equals(parsed[0])) {
					int[] gameStatePositions = readPositions(parsed, 1);
					render(gameStatePositions);
				}
			});
		}
	}

	private void render(int[] gameStatePositions) {
		flushBoard();
		flushAnimals();

		int sheepR = gameStatePositions[0];
		int sheepC = gameStatePositions[1];

		moveAnimal(board.getSheep(), sheepR, sheepC);

		for (int i = 2, w = 0; i < 2 + 4 * 2; i += 2, w++) {
			int wolfR = gameStatePositions[i];
			int wolfC = gameStatePositions[i + 1];
			Wolf wolf = board.getWolf()[w];
			moveAnimal(wolf, wolfR, wolfC);
		}
	}

	private void moveAnimal(Shape animal, int r, int c) {
		board.getBoardSquares(r, c)
				.getChildren()
				.add(animal);
		animal.setColPosition(c);
		animal.setRowPosition(r);
	}

	private void flushAnimals() {
		Sheep sheep = board.getSheep();
		flushAnimal(sheep);
		for (Wolf wolf : board.getWolf()) {
			flushAnimal(wolf);
		}
	}

	private void flushAnimal(Shape animal) {
		board.getBoardSquares(animal.getRowPosition(), animal.getColPosition())
				.getChildren()
				.remove(animal);
	}


	private int[] readPositions(String[] parsed, int offset) {
		int[] positions = new int[parsed.length - offset];
		for (int i = offset; i < parsed.length; i++) {
			positions[i - offset] = Integer.parseInt(parsed[i]);
		}
		return positions;
	}

	private void highlightPossibleMoves(int wolf, int[] possibleMovesCoords) {
		flushBoard();
		for (int i = 0; i < possibleMovesCoords.length; i += 2) {
			int nextR = i;
			int nextC = i + 1;
			BoardSquare boardSquare = board.getBoardSquares(possibleMovesCoords[nextR], possibleMovesCoords[nextC]);
			boardSquare.highlight(Color.YELLOWGREEN);
			boardSquare.setOnMouseClicked(mouse -> {
				out.print(Command.MOVE_WOLF.name());
				out.print(":");
				out.print(wolf);
				out.print(":");
				out.println(possibleMovesCoords[nextC]);
				out.flush();
			});
		}
	}

	private void flushBoard() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				board.getBoardSquares(i, j).clear();
				board.getBoardSquares(i, j).setOnMouseClicked(null);
			}
		}
	}

	private void showWinner(Shape.MonsterType winner) {
		String message = getWinnerMessage(winner);

		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("игра окончена");
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.setOnCloseRequest(event -> {
			System.exit(0);
		});
		alert.showAndWait();
	}


	private String getWinnerMessage(Shape.MonsterType winner) {
		if (winner == Shape.MonsterType.WOLF) {
			return "Волки победили!";
		}
		return "Овечки победили!!!";
	}

	private void bindWolfs() {
		Wolf[] boardWolf = board.getWolf();
		for (int i = 0, boardWolfLength = boardWolf.length; i < boardWolfLength; i++) {
			Wolf wolf = boardWolf[i];
			int currentWolfIndex = i;
			wolf.setOnMouseClicked(mouse -> {
				out.print(Command.SELECT_WOLF.name());
				out.print(":");
				out.println(currentWolfIndex);
				out.flush();
			});
		}
	}

	public Board getBoard() {
		return board;
	}

	@Override
	public void run() {
		try {
			start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
